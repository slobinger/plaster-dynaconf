# -*- coding: utf-8 -*-
import setuptools

# read the contents of your README file
from os import path

def get_long_description():
    this_directory = path.abspath(path.dirname(__file__))
    with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
        return f.read()


setuptools.setup(
    name='plaster-dynaconf',
    version='0.1.0',
    description='A plaster plugin to configure pyramid app with Dynaconf',
    long_description=get_long_description(),
    long_description_content_type='text/markdown',
    author='Sebastian Lobinger',
    author_email='software.slobinger@t-online.de',
    url='https://gitlab.com/lobi/plaster-dynaconf',
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    install_requires=(
        'Dynaconf>=3.0',
        'plaster>=1.0',
    ),
    entry_points={
        'plaster.loader_factory': ('dynaconf = plaster_dynaconf:Loader'),
    },
    python_requires=">=3.6",
)
